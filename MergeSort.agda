module MergeSort where

open import Data.Bool using (Bool; false; true)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.Nat
open import Data.Nat.Properties
open import Data.Product
open import Data.Sum
open import Data.Vec
open import Function
open import Relation.Binary.PropositionalEquality
open import Relation.Nullary

--------------------------------------------------------------------------------
-- Naturals as binary strings (from PLFA)

-- Binary strings
data BitString : Set where
  ⟨⟩ : BitString
  _O : BitString → BitString
  _I : BitString → BitString

-- `BitString`s with a leading I
data One : BitString → Set where
  one⟨⟩ : One (⟨⟩ I)
  oneO : ∀ {b} → One b → One (b O)
  oneI : ∀ {b} → One b → One (b I)

-- Canonical form `BitString`s.
-- A subset of `BitString`s with a canonical 1-to-1 correspondence with ℕ
data Bin : BitString → Set where
  zeroBits : Bin (⟨⟩ O)
  oneBits : {b : BitString} (o : One b) → Bin b


-- Increment a `BitString`
suc-BitString : BitString → BitString
suc-BitString ⟨⟩ = ⟨⟩ I
suc-BitString (b O) = b I
suc-BitString (b I) = (suc-BitString b) O

-- `BitString`s with a leading I are preserved by incrementing
suc-One : ∀ {b} → One b → One (suc-BitString b)
suc-One one⟨⟩ = oneO one⟨⟩
suc-One (oneO o) = oneI o
suc-One (oneI o) = oneO (suc-One o)

suc-Bin′ : ∀ {b} → Bin b → One (suc-BitString b)
suc-Bin′ zeroBits = one⟨⟩
suc-Bin′ (oneBits o) = suc-One o

-- Canonical form `BitString`s are preserved by incrementing
suc-Bin : ∀ {b} → Bin b → Bin (suc-BitString b)
suc-Bin = oneBits ∘ suc-Bin′

-- Convert a natural to a `BitString`.
toBits : ℕ → BitString
toBits zero = ⟨⟩ O
toBits (suc n) = suc-BitString (toBits n)

-- `BitString`s converted from naturals are in canonical form.
toBits-Bin : ∀ n → Bin (toBits n)
toBits-Bin zero = zeroBits
toBits-Bin (suc n) = suc-Bin (toBits-Bin n)


-- Convert a `BitString` to a natural.
fromBits : BitString → ℕ
fromBits ⟨⟩ = zero
fromBits (b O) = fromBits b + fromBits b
fromBits (b I) = suc (fromBits b + fromBits b)

-- `fromBits` commutes with incrementing
lift-suc-BitString : ∀ b → fromBits (suc-BitString b) ≡ suc (fromBits b)
lift-suc-BitString (⟨⟩) = refl
lift-suc-BitString (b O) = refl
lift-suc-BitString (b I)
  rewrite lift-suc-BitString b | +-suc (fromBits b) (fromBits b)= refl

-- One half of the proof that fromBits and toBits are inverses of each other
-- (for canonical form `BitString`s)
fromBits∘toBits≡id : ∀ n → fromBits (toBits n) ≡ n
fromBits∘toBits≡id (zero) = refl
fromBits∘toBits≡id (suc n)
  rewrite lift-suc-BitString (toBits n) | fromBits∘toBits≡id n = refl

-- Helper function
-- Converting a non-zero natural n = 2m to a BitString gives the same result as converting m then appending O
double-ℕ-to-BitString : (n : ℕ) → toBits (suc n + suc n) ≡ (toBits (suc n)) O
double-ℕ-to-BitString zero = refl
double-ℕ-to-BitString (suc n) rewrite +-suc n (suc n) | double-ℕ-to-BitString n = refl

-- Find the natural that corresponds to a non-zero `BitString`
viewBitsAsℕ : ∀ {b} → One b → ∃ λ (n : ℕ) → b ≡ toBits (suc n)
viewBitsAsℕ one⟨⟩ = zero , refl
viewBitsAsℕ (oneO o) with viewBitsAsℕ o
... | n , refl rewrite sym (double-ℕ-to-BitString n) | +-suc n n = suc (n + n) , refl
viewBitsAsℕ (oneI o) with viewBitsAsℕ o
... | n , refl rewrite cong suc-BitString (sym (double-ℕ-to-BitString n)) | +-suc n n = suc (suc (n + n)) , refl

-- One half of the proof that fromBits and toBits are inverses of each other
-- (for canonical form `BitString`s)
toBits∘fromBits≡id : ∀ {b} → Bin b → toBits (fromBits b) ≡ b
toBits∘fromBits≡id zeroBits = refl
toBits∘fromBits≡id (oneBits one⟨⟩) = refl
toBits∘fromBits≡id (oneBits (oneO {b} o)) with viewBitsAsℕ o
... | n , refl rewrite fromBits∘toBits≡id (suc n) | double-ℕ-to-BitString n = refl
toBits∘fromBits≡id (oneBits (oneI {b} o)) with viewBitsAsℕ o
... | n , refl rewrite fromBits∘toBits≡id (suc n) | double-ℕ-to-BitString n = refl

--------------------------------------------------------------------------------

min : ℕ → ℕ → ℕ
min m n with ≤-total m n
... | inj₁ m≤n = m
... | inj₂ n≤m = n

min-× : ∀ {m n p} → m ≤ n → m ≤ p → m ≤ min n p
min-× {m} {n} {p} m≤n m≤p with ≤-total n p
... | inj₁ n≤p = m≤n
... | inj₂ p≤n = m≤p

--------------------------------------------------------------------------------

-- Sorted non-empty vectors indexed by least element
data Sorted1 : (length : ℕ) → (min : ℕ) → Set where
  one : (x : ℕ) → Sorted1 1 x
  _∷_ : ∀ {n} {x y} → x ≤ y → Sorted1 n y → Sorted1 (suc n) x

fromSorted1 : ∀ {n} {x} → Sorted1 n x → Vec ℕ n
fromSorted1 (one x) = x ∷ []
fromSorted1 (_∷_ {x = x} _ ps) = x ∷ fromSorted1 ps

-- Sorted vectors indexed by least element
data Sorted : (length : ℕ) → Set where
  [] : Sorted 0
  sorted1 : ∀ {n} {min : ℕ} → Sorted1 (suc n) min → Sorted (suc n)

fromSorted : ∀ {n} → Sorted n → Vec ℕ n
fromSorted [] = []
fromSorted (sorted1 ps) = fromSorted1 ps

--------------------------------------------------------------------------------
-- Merge-ish sort!

-- Combine two sorted, non-empty vectors into a sorted, non-empty vector
mergeSorted1 : ∀ {x y} {n m} → Sorted1 n x → Sorted1 m y → Sorted1 (n + m) (min x y)
mergeSorted1 {x} {y} {.1} {.1} (one .x) (one .y) with ≤-total x y
... | inj₁ x≤y = x≤y ∷ one y
... | inj₂ y≤x = y≤x ∷ one x
mergeSorted1 {x} {y} {.1} {.(suc _)} (one .x) (q ∷ qs) with ≤-total x y
... | inj₁ x≤y = x≤y ∷ q ∷ qs
... | inj₂ y≤x = min-× y≤x q ∷ mergeSorted1 (one x) qs
mergeSorted1 {x} {y} {suc (suc n)} {.1} (p ∷ ps) (one .y) with ≤-total x y
... | inj₁ x≤y = min-× p x≤y ∷ mergeSorted1 ps (one y)
... | inj₂ y≤x rewrite +-comm n 1 = y≤x ∷ p ∷ ps
mergeSorted1 {x} {y} {suc (suc n)} {suc (suc m)} (p ∷ ps) (q ∷ qs) with
  mergeSorted1 ps (q ∷ qs) | mergeSorted1 (p ∷ ps) qs | ≤-total x y
... | tail | _ | inj₁ x≤y = min-× p x≤y ∷ tail
... | _ | tail | inj₂ y≤x rewrite +-suc n (suc m) = min-× y≤x q ∷ tail

-- Combine two sorted vectors into a sorted vector
mergeSorted : ∀ {n m} → Sorted n → Sorted m → Sorted (n + m)
mergeSorted {n} [] [] = []
mergeSorted {n} [] (sorted1 ps) = sorted1 ps
mergeSorted {n} (sorted1 ps) [] rewrite +-identityʳ n = sorted1 ps
mergeSorted {n} {m} (sorted1 ps) (sorted1 qs) = sorted1 (mergeSorted1 ps qs)

-- Insert an element into a sorted vector returning a sorted vector
insertSorted : ∀ {n} → (x : ℕ) → Sorted n → Sorted (suc n)
insertSorted x ps = mergeSorted (sorted1 (one x)) ps

-- My sorting function (merge sort but worse)
mySort′ : ∀ {b} → Bin b → Vec ℕ (fromBits b) → Sorted (fromBits b)
mySort′ {.(⟨⟩ O)} zeroBits [] = []
mySort′ {.(⟨⟩ I)} (oneBits one⟨⟩) (x ∷ []) = sorted1 (one x)
mySort′ {b O} (oneBits (oneO o)) xs with splitAt (fromBits b) {fromBits b} xs
... | ys , zs , refl = let ys′ = (mySort′ {b} (oneBits o) ys)
                           zs′ = (mySort′ {b} (oneBits o) zs)
                       in  mergeSorted ys′ zs′
mySort′ {b I} (oneBits (oneI o)) (x ∷ xs) with splitAt (fromBits b) {fromBits b} xs
... | ys , zs , refl =
  let ys′ = mySort′ (oneBits o) ys
      zs′ = mySort′ (oneBits o) zs
  in  insertSorted x (mergeSorted ys′ zs′)

-- A convenient wrapper for my sorting function
mySort : ∀ {n} → Vec ℕ n → Sorted n
mySort {n} xs rewrite sym (fromBits∘toBits≡id n) = aux n xs
  where
  aux : ∀ n → Vec ℕ (fromBits (toBits n)) → Sorted (fromBits (toBits n))
  aux n xs = mySort′ {toBits n} (toBits-Bin n) xs


-- Unit tests in the type system!

test0 : fromSorted (mySort []) ≡ []
test0 = refl

test1 : fromSorted (mySort (1 ∷ [])) ≡ (1 ∷ [])
test1 = refl

test2 : fromSorted (mySort (3 ∷ 2 ∷ 1 ∷ [])) ≡ 1 ∷ 2 ∷ 3 ∷ []
test2 = refl

test3 : fromSorted (mySort (3 ∷ 2 ∷ 1 ∷ 2 ∷ 0 ∷ 5 ∷ 4 ∷ [])) ≡ 0 ∷ 1 ∷ 2 ∷ 2 ∷ 3 ∷ 4 ∷ 5 ∷ []
test3 = refl
